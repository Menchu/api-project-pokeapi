// //búsqueda  pokemon

// const fetchPokemonSearch = (pokeName) =>{
//   const url = `https://pokeapi.co/api/v2/pokemon/${pokeName}`;
//   fetch(url)
//       .then((response) =>{
//           return response.json();
//       })
//       .then((pokemon) =>{
//           displayPokemonDetail(pokemon);
//       })
//       .catch((error) =>{
//           const pokedex = document.getElementById("PokeList");
//           const pokemonHTMLString =
//           `
//          <div class="sectionUl__pokeUl">
//           <p class="sectionUl__pokeUl__error">error: Hubo un problema con la peticion de fetch ${error.message}</p>
//           </div>

//           `
//           pokedex.innerHTML = pokemonHTMLString;
//       });
// };  



//pintar detalle
// const displayPokemonDetail = (pokemon) =>{
//   const pokedex = document.getElementById("PokeList");
//   console.log(pokemon);
//   const pokeType = pokemon.types.map((type) => type.type.name).join("- ");
//   console.log(pokeType);
//   const pokemonHTMLString = 
//   `
//   <li class="flex-item">
//       <img class="flex-item-image" src="${pokemon.sprites["front_default"]}"/>
//       <h2 class="flex-item-title">${pokemon.id}. ${pokemon.name}</h2>
//       <p class="flex-item-subtitle">Type: ${pokeType} </p>
//   </li>
//   `
//   pokedex.innerHTML = pokemonHTMLString;
// };

const fetchPokemonSearch = async (pokeName) => {
    const url = `https://pokeapi.co/api/v2/pokemon/${pokeName}/`;
    
    const res = await fetch(url).catch((error) => {
      const pokedex = document.getElementById("PokeList");
      const pokemonHTMLString = `
      <div class="flex-item">
          <p class="flex-item-error">Error: Hubo un problema con la petición Fetch: '${error.message}</p>
      </div>`;
      pokedex.innerHTML = pokemonHTMLString;
    });
    const data = await res.json();
   
    let pokemon = {
      name: data.name,
      image: data.sprites["front_default"],
      type: data.types.map((type) => type.type.name).join(", "),
      id: data.id,
      attack: data,
      baseExperience:data.base_experience,
    };
    return pokemon;
  };
  
  export { fetchPokemonSearch };
  