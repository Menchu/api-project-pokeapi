
// const fetchAllPokemon = () => {
//   //contenedor vacío
//   const promises = [];
//   // buscamos 150 pokemon
//   for (let i = 1; i <= 150; i++) {
//     const url = `https://pokeapi.co/api/v2/pokemon/${i}`;
//     //añadimos a nuestro array los fetch en forma de json
//     promises.push(fetch(url).then((res) => res.json()));
//   }

//   Promise.all(promises).then((results) => {
//     //guardamos respuesta en pokemon y mapeamos
//     const pokemon = results.map((result) => ({
//       name: result.name,
//       image: result.sprites["front_default"],
//       type: result.types.map((type) => type.type.name).join(", "),
//       id: result.id,
//     }));
//     console.log(pokemon)
//     displayPokemon(pokemon);//llamando función de pintado

//   });
//   document.getElementById("PokeList").classList.remove("hidden");

// };


// //pintar lista
// const displayPokemon = (pokemon) => {

//   const pokedex = document.getElementById("PokeList");

//   const pokemonHTMLString = pokemon
//     .map(
//       (pokemonDetail) =>

//         `<li class="mainLI">
//      <img class="imgMain" src="${pokemonDetail.image}"> 
//      <h2>${pokemonDetail.id}. ${pokemonDetail.name}</h2>
//      <p class="imgMain">Type: ${pokemonDetail.type}</p> 
//      </li>
//      `

//     )
//     .join("");

//   pokedex.innerHTML = pokemonHTMLString;
// };



const fetchPokemon = async () => {
  let allPokemons = [];
  for (let i = 1; i <= 150; i++) { 
    const url = `https://pokeapi.co/api/v2/pokemon/${i}`;
    const res = await fetch(url).catch((error) => {
      const pokedex = document.getElementById("PokeList");
      const pokemonHTMLString = `
      <div class="flex-item">
          <p class="flex-item-error">Error: Hubo un problema con la petición Fetch: '${error.message}</p>
      </div>`;
      pokedex.innerHTML = pokemonHTMLString;
    });
    const data = await res.json();
    let pokemon = {
        name: data.name,
        image: data.sprites["front_default"],
        type: data.types.map((type) => type.type.name).join(", "),
        id: data.id,
    };
    allPokemons.push(pokemon);
  }
  return allPokemons
};

export { fetchPokemon };
