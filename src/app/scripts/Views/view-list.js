import { fetchPokemon } from "../Api/api-list";

const displayPokemon = () => {
  const pokedex = document.getElementById("PokeList");
  const pokemon = fetchPokemon();
  pokemon.then((pokemons) => {
    const pokemonHTMLString = pokemons.map((pokemon) => `
    <li id="${pokemon.id}" class="flex-item">
        <img class="flex-item-image" src="${pokemon.image}"/>
        <h2 class="flex-item-title">${pokemon.id}. ${pokemon.name}</h2>
        <p class="flex-item-subtitle">Type: ${pokemon.type}</p>
    </li>
    `
    )
    .join("");
    PokeList.innerHTML = pokemonHTMLString;
  });
};

export { displayPokemon };

  