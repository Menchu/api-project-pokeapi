import { fetchPokemonSearch } from '../Api/api-search-detail'
import { PokeRandomizer } from '../Class/class';
import { PokeUtil } from '../Class/utilClass';


const displayPokemonRandom = async () => {
    const pokedex = document.getElementById("PokeList");
    const pokeId = Math.round(Math.random() * (150 - 1) + 1);
    const pokemon = await fetchPokemonSearch(pokeId);
    const myPokemonClass = new PokeRandomizer (pokemon.id, pokemon.name, pokemon.image, pokemon.type, pokemon.baseExperience);
    let level = PokeUtil.pokeLevel(pokemon.baseExperience);

    const pokemonHTMLString =
        `<li class="flex-item">
            <img class="flex-item-image" src="${ myPokemonClass.getPokemonImg() }"/>
            <h2 class="flex-item-title">${ myPokemonClass.getPokemonId()}. ${myPokemonClass.getPokemonName() }</h2>
            <p class="flex-item-subtitle">Type: ${ myPokemonClass.getPokemonType() }</p>
            <p class="flex-item-subtitle">PokeExperience: ${ level }</p>
        </li>`
    pokedex.innerHTML = pokemonHTMLString;
};

export { displayPokemonRandom }
