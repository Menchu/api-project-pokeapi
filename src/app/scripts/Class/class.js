class PokeClass {
    constructor(id, name, image, type) {
      this.id = id;
      this.name = name;
      this.image = image;
      this.type = type;
    }
    getPokemonId() {
      return this.id;
    }
  
    getPokemonName() {
      return this.name;
    }
  
    getPokemonImg() {
      return this.image;
    }
  
    getPokemonType() {
      return this.type;
    }
  
    setPokemonAlias(pokeAlias) {
      this.name = pokeAlias;
    }
  }
  
  class PokeDetailClass extends PokeClass {
    constructor(id, name, image, type, attacks) {
      super(id, name, image, type);
      this.attacks = attacks;
    }
    getPokemonAttacks() {
      return this.attacks;
    }
  }
  
  class PokeRandomizer extends PokeClass {
    constructor(id, name, image, type, baseExperience) {
      super(id, name, image, type);
      this.baseExperience = baseExperience;
    }
    getPokeExperience() {
      return this.baseExperience;
    }
  
  }
  
  export { PokeClass, PokeDetailClass, PokeRandomizer };
  