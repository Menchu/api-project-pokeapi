import '../app/styles/styles.scss'; 
import { displayPokemon } from '../app/scripts/Views/view-list';
import { displayPokemonDetail } from '../app/scripts/Views/view-detail';
import { displayPokemonRandom } from '../app/scripts/Views/view-random';



let vistaHome =() =>{ 
   
    document.getElementById("home").classList.remove("hidden");
    document.getElementById("PokeList").classList.add("hidden");
}


let viewContactMe =() =>{
    document.getElementById("contact").classList.remove("hidden");
    document.getElementById("contact").classList.add("section__contact");

}
let viewAbout =() =>{
    document.getElementById("about").classList.remove("hidden");
    document.getElementById("about").classList.add("section__about");

}


// Funcion para añadir listeners en el html
function addListeners() {
   
    document.getElementById("anchorHome").addEventListener("click",vistaHome);
    document.getElementById("contact").addEventListener("click",viewContactMe);
    document.getElementById("about").addEventListener("click",viewAbout);


    document.getElementById("contact").addEventListener("click",viewContactMe);
    document.getElementById("btnList").addEventListener("click", displayPokemon);
    document.getElementById("submit").addEventListener("click", displayPokemonDetail);
    document.getElementById("btnRandom").addEventListener("click", displayPokemonRandom);
  
}

window.onload = function () {
    // Añade Eventos -> Para que la ventana escuche eventos
    addListeners();
    
  };
